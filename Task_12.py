from random import randint


def list_generation():
    a = []
    b = []

    for i in range(15):
        a.append(randint(10, 30))
        b.append(randint(10, 30))
    print(f'Первый случайный список - {a}\n'
          f'Второй случайный список - {b}\n')

    c = list(set(a) & set(b))
    print(f'Список повторяющихся чисел - {c}')


list_generation()
